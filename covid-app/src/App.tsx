import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from "react-router-dom";
import "leaflet/dist/leaflet.css";
import Login from "./components/Login";
import Admin from "./components/Admin";

function App() {
  return (
    <div className="App">
      <Router>
        <Redirect exact from="/" to="/login"></Redirect>
        <Route path="/login">
          <Login />
        </Route>
        <Route path="/admin">
          <Admin />
        </Route>
      </Router>
    </div>
  );
}

export default App;
