import React from "react";
import { Col, Card } from "react-bootstrap";

interface ICardsProps {
  bg: string;
  title: string;
  data: number;
  img: string;
}

const Cards = (props: ICardsProps) => {
  const { bg, title, data, img } = props;
  const numberWithCommas = (x: number) => {
    return x?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };
  return (
    <>
      <Col xs={4}>
        <Card bg={bg} className="text-left text-light">
          <Card.Body>
            <Card.Title>
              {" "}
              <i className={`mr-2 ${img}`}></i>
              {title}
            </Card.Title>
            <Card.Text>{numberWithCommas(data)}</Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </>
  );
};

export default Cards;
