export interface Countries extends Brief {
  countryregion: string;
  lastupdate: string;
  location: Location;
  countrycode: CountryCode;
}

export interface Brief {
  confirmed: number;
  deaths: number;
  recovered: number;
}

interface Location {
  lat: number;
  lng: number;
}

interface CountryCode {
  iso2: string;
  iso3: string;
}
