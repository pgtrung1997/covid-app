import React, { useState } from "react";
import { Field, Formik, Form } from "formik";
import { Container, Button } from "react-bootstrap";
import { useHistory } from "react-router";
import * as Yup from "yup";
import axios from "axios";
import style from "./css/Login.module.css";

interface Values {
  email: string;
  password: string;
  confirm: string;
}

const Login = () => {
  const [showPass, setShowPass] = useState<boolean>(false);
  let history = useHistory();
  const showPassword = () => {
    if (showPass) {
      setShowPass(false);
    } else {
      setShowPass(true);
    }
  };

  const errorMessage = Yup.object().shape({
    email: Yup.string().email("Invalid Email").required("Email is required"),
    password: Yup.string().required("Password is required"),
    confirm: Yup.string()
      .required("Password is required")
      .oneOf(
        [Yup.ref("password"), null],
        "Confirm password must equal password"
      ),
  });

  const initialValues = {
    email: "",
    password: "",
    confirm: "",
  };
  return (
    <>
      <Container
        fluid
        className={`text-left border rounded px-4 py-5 w-25 ${style["center"]}`}
      >
        <Formik
          initialValues={initialValues}
          validationSchema={errorMessage}
          onSubmit={async (values: Values) => {
            try {
              const res = await axios.post("/api/login", {
                email: values.email,
                password: values.password,
              });

              localStorage.setItem("myToken", res.data.user.token);
              localStorage.setItem("user", values.email);
              history.push("/admin");
            } catch (err) {
              console.log(err);
            }
          }}
        >
          {({ errors, touched, values }) => (
            <Form>
              <div className="form-group">
                <h2 className="mb-4 text-center">Login</h2>
                <label htmlFor="email">Email address</label>
                <Field
                  type="email"
                  id="email"
                  name="email"
                  className="form-control"
                  placeholder="Enter email"
                />
                {touched.email && errors.email && (
                  <div className="text-danger">{errors.email}</div>
                )}
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <Field
                  type={showPass ? "text" : "password"}
                  id="password"
                  name="password"
                  className="form-control"
                  placeholder="Enter password"
                />
                {touched.password && errors.password && (
                  <div className="text-danger">{errors.password}</div>
                )}
              </div>
              <div className="form-group">
                <label htmlFor="confirm">Confirm Password</label>
                <Field
                  type={showPass ? "text" : "password"}
                  id="confirm"
                  name="confirm"
                  className="form-control"
                  placeholder="Re-enter password"
                />
                {touched.confirm && errors.confirm && (
                  <div className="text-danger">{errors.confirm}</div>
                )}
              </div>
              <div className="form-group form-check">
                <label className="form-check-label">
                  <Field
                    type="checkbox"
                    className="form-check-input"
                    name="toggle"
                    onClick={showPassword}
                  />
                  Check to display password
                </label>
              </div>
              <Button type="submit">Submit</Button>
            </Form>
          )}
        </Formik>
      </Container>
    </>
  );
};

export default Login;
