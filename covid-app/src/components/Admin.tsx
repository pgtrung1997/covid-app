import React, { useEffect, useState } from "react";
import { Button, Container, Row, Col, Form } from "react-bootstrap";
import { useHistory } from "react-router";
import Cards from "./Cards";
import CovidMap from "./CovidMap";
import axios from "axios";
import { Brief, Countries } from "./datatype";

const Admin = () => {
  const history = useHistory();
  const [brief, setBrief] = useState<Brief>({
    confirmed: 0,
    deaths: 0,
    recovered: 0,
  });
  const [countries, setCountries] = useState<Countries[]>([]);
  const [currentCountry, setCurrentCountry] = useState<any>();

  const logOut = async () => {
    if (localStorage.getItem("myToken")) {
      try {
        await axios.post("/api/logout", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("myToken")}`,
          },
        });
        localStorage.clear();
        history.push("/login");
      } catch (err) {
        console.log(err);
      }
    }
  };

  useEffect(() => {
    if (localStorage.getItem("myToken")) {
      const callApi = async () => {
        const getBrief = axios.get("/api/brief", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("myToken")}`,
          },
        });
        const getCountries = axios.get("/api/countries", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("myToken")}`,
          },
        });
        const getTime = axios.get("/api/timeseries", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("myToken")}`,
          },
        });

        await axios.all([getBrief, getCountries, getTime]).then(
          axios.spread((getBrief, getCountries, getTime) => {
            setBrief(getBrief.data.brief);
            setCountries(getCountries.data.countries);
            let value = getCountries.data.countries.filter((v: Countries) => {
              return v.countryregion === "Vietnam";
            });

            setCurrentCountry(value[0]);
          })
        );
      };
      callApi();
    }
  }, []);

  return (
    <>
      <Container fluid>
        <Row className="header mt-3 justify-content-between">
          <Col xs={3}>
            <h4>Covid Dashboard</h4>
          </Col>
          <Col xs={3} className="user">
            <span className="mr-5">{localStorage.getItem("user")}</span>
            <Button variant="secondary" onClick={logOut}>
              Logout
            </Button>
          </Col>
        </Row>
      </Container>

      <Container className="mt-5">
        <Row className="mb-3">
          <Col className="text-left justify-content-start">
            <h4>World Wide</h4>
          </Col>
        </Row>
        <Row className="justify-content-between mb-3">
          <Cards
            bg={"primary"}
            title={"Confirmed"}
            data={brief?.confirmed}
            img={"bi bi-person-fill"}
          />
          <Cards
            bg={"info"}
            title={"Recovered"}
            data={brief?.recovered}
            img={"bi bi-heart-fill"}
          />
          <Cards
            bg={"danger"}
            title={"Deaths"}
            data={brief?.deaths}
            img={"bi bi-exclamation-octagon-fill"}
          />
        </Row>

        <Row className="d-flex justify-content-start align-items-center mb-3">
          <Col xs={1} className="text-left mr-3">
            <h4>Regional</h4>
          </Col>
          <Col xs={3} className="text-left">
            <Form.Group controlId="ControlSelect">
              <Form.Label>Country & Region</Form.Label>
              <Form.Control
                className="border-top-0 border-left-0 border-right-0"
                value={currentCountry?.countryregion}
                as="select"
                onChange={async (e: any) => {
                  let country = countries.filter((v: Countries) => {
                    return v.countryregion === e.target.value;
                  });

                  setCurrentCountry(country[0]);
                }}
              >
                {countries.map((v: Countries, i: number) => {
                  return (
                    <option key={i} value={v.countryregion}>
                      {v.countryregion}
                    </option>
                  );
                })}
              </Form.Control>
            </Form.Group>
          </Col>
        </Row>
        <Row className="justify-content-between mb-3">
          <Cards
            bg={"primary"}
            title={"Confirmed"}
            data={currentCountry?.confirmed}
            img={"bi bi-person-fill"}
          />
          <Cards
            bg={"info"}
            title={"Recovered"}
            data={currentCountry?.recovered}
            img={"bi bi-heart-fill"}
          />
          <Cards
            bg={"danger"}
            title={"Deaths"}
            data={currentCountry?.deaths}
            img={"bi bi-exclamation-octagon-fill"}
          />
        </Row>
        <Row className="mb-3">
          <Col>
            <CovidMap countries={countries} />
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Admin;
