import { LatLngExpression } from "leaflet";
import React from "react";
import { MapContainer, TileLayer, Circle } from "react-leaflet";
import style from "./css/Map.module.css";
import { Countries } from "./datatype";

interface ICovidMapProps {
  countries: Countries[];
}

const CovidMap = (props: ICovidMapProps) => {
  const { countries } = props;
  const position: LatLngExpression = [51.505, -0.09];

  return (
    <MapContainer
      className={style["leaflet-container"]}
      center={position}
      zoom={4}
    >
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />

      {countries.map((v) => {
        return (
          <Circle
            key={v.countryregion}
            center={v.location}
            fillColor="red"
            stroke={false}
            radius={v.confirmed * 0.3}
          />
        );
      })}
    </MapContainer>
  );
};

export default CovidMap;
